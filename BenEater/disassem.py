opCode={0x69:("    adc #${}", 2),
        0x65:("    adc ${}", 2),
        0x75:("    adc ${},x", 2),
        0x6d:("    adc ${}", 3),
        0x7d:("    adc ${}.x", 3),
        0x79:("    adc ${},y", 3),
        0x61:("    adc (${},x)", 2),
        0x71:("    adc (${}),y", 2),
        0x29:("    and #${}", 2),
        0x25:("    and ${}", 2),
        0x35:("    and ${},x", 2),
        0x2d:("    and ${}", 3),
        0x3d:("    and ${},x", 3),
        0x39:("    and ${},y", 3),
        0x21:("    and (${},x)", 2),
        0x21:("    and (${},y)", 2),
        0x0a:("    asl", 1),
        0x06:("    asl ${}", 2),
        0x16:("    asl ${},x", 2),
        0x0e:("    asl ${}", 3),
        0x1e:("    asl ${},x", 3),
        0x10:("    bpl", 2),
        0x30:("    bmi", 2),
        0x50:("    bvc", 2),
        0x70:("    bvs", 2),
        0x90:("    bcc", 2),
        0xb0:("    bcs", 2),
        0xd0:("    bne", 2),
        0xf0:("    beq", 2),
        0x00:("    brk", 1),
        0xc9:("    cmp #${}",2),
        0xc5:("    cmp ${}",2),
        0xd5:("    cmp ${},x",2),
        0xcd:("    cmp ${}",3),
        0xdd:("    cmp ${},x",3),
        0xd9:("    cmp ${},y",3),
        0xc1:("    cmp (${},x)",2),
        0xd1:("    cmp (${}),y",2),
        0xe0:("    cpx #${}",2),
        0xe4:("    cpx ${}",2),
        0xec:("    cpx ${}",3),
        0xc0:("    cpy #${}",2),
        0xc4:("    cpy ${}",2),
        0xcc:("    cpy ${}",3),
        0xc6:("    dec ${}",2),
        0xd6:("    dec ${},x",2),
        0xce:("    dec ${}",3),
        0xde:("    dec ${},y",3),
        0x49:("    eor #${}",2),
        0x45:("    eor ${}",2),
        0x55:("    eor ${},x",2),
        0x4d:("    eor ${}",3),
        0x5d:("    eor ${},x",3),
        0x59:("    eor ${},y",3),
        0x41:("    eor (${},x)",2),
        0x51:("    eor (${}),y",2),
        0x18:("    clc",1),
        0x38:("    sec",1),
        0x58:("    cli",1),
        0x78:("    sei",1),
        0xb8:("    clv",1),
        0xd8:("    cld",1),
        0xf8:("    sed",1),
        0xe6:("    inc ${}",2),
        0xf6:("    inc ${},x",2),
        0xee:("    inc ${}",3),
        0xfe:("    inc ${},x",3),
        0x4c:("    jmp ${}",3),
        0x6c:("    jmp $({})",3),
        0x20:("    jsr ${}",1),
        0xa9:("    lda #{}",2),
        0xa5:("    lda ${}",2),
        0xb5:("    lda ${},x",2),
        0xad:("    lda ${}",3),
        0xbd:("    lda ${},x",3),
        0xb9:("    lda ${},y",3),
        0xa1:("    lda (${},x)",2),
        0xb1:("    lda (${}),y",2),
        0xa2:("    ldx #${}",2),
        0xa6:("    ldx ${}",2),
        0xb6:("    ldx ${},y",2),
        0xae:("    ldx ${}",3),
        0xbe:("    ldx ${},y ",3),
        0xa0:("    ldy #${}",2),
        0xa4:("    ldy ${}",2),
        0xb4:("    ldy ${},x",2),
        0xac:("    ldy ${}",3),
        0xbc:("    ldy ${},x ",3),
        0x4a:("    lsr",1),
        0x46:("    lsr ${}",2),
        0x56:("    lsr ${},x",2),
        0x4e:("    lsr ${}",3),
        0x5e:("    lsr ${},x",3),
        0xea:("    nop",1),
        0x09:("    ora #${}   ",2),
        0x05:("    ora ${}    ",2),
        0x15:("    ora ${},x  ",2),
        0x0d:("    ora ${}  ",3),
        0x1d:("    ora ${},x",3),
        0x19:("    ora ${},y",3),
        0x01:("    ora (${},x)",2),
        0x11:("    ora (${}),y",2),
        0xaa:("    tax",1),
        0x8a:("    txa",1),
        0xca:("    dex",1),
        0xe8:("    inx",1),
        0xa8:("    tay",1),
        0x98:("    tya",1),
        0x88:("    dey",1),
        0xc8:("    iny",1),
        0x2a:("    rol",1),
        0x26:("    rol ${}",2),
        0x36:("    rol ${},x",2),
        0x2e:("    rol ${}",3),
        0x3e:("    rol ${},x",3),
        0x6a:("    ror",1),
        0x66:("    ror ${}",2),
        0x76:("    ror ${}},x",2),
        0x6e:("    ror ${}",3),
        0x7e:("    ror ${},x",3),
        0x40:("    rti",1),
        0x60:("    rts",1),
        0xe9:("    sbc",2),
        0xe5:("    sbc",2),
        0xf5:("    sbc",2),
        0xed:("    sbc",3),
        0xfd:("    sbc",3),
        0xf9:("    sbc",3),
        0xe1:("    sbc",2),
        0xf1:("    sbc",2),
        0x85:("    sta ${}",2),
        0x95:("    sta ${},x",2),
        0x8D:("    sta ${}",3),
        0x9D:("    sta ${},x",3),
        0x99:("    sta ${},y",3),
        0x81:("    sta (${},x)",2),
        0x91:("    sta (${}),y",2),
        0x9a:("    txs",1),
        0xba:("    tsx",1),
        0x48:("    pha",1),
        0x68:("    pla",1),
        0x08:("    php",1),
        0x28:("    plp",1),
        0x86:("    stx ${}",2),
        0x96:("    stx ${},y",2),
        0x8e:("    stx ${}",3),
        0x84:("    sty ${}",2),
        0x94:("    sty ${},x",2),
        0x8c:("    sty ${}",3),

}


code = bytearray([
   0xa9,0x01,         #  lda #$01
   0x85,0x00,         #  sta $00
   0xd0,0x03,         #  bne $0609
   0x4c,0x00,0x06,    #  jmp $0600
   0xa2,0x01,         #  ldx #$01
   0x4c,0x00,0x06])   #  jmp $0600


def isBranch(opCode):
    if opCode in [0x10, 0x30, 0x50, 0x70, 0x90, 0xb0, 0xd0, 0xf0,]:
       return True
    else:
       return False


n = 0
addr = 0x600

print("code: ",code)
print(" Len:",len(code) - 1)
print("Waiting....")
input()

while (n <= len(code)-1):
    byte = code[n]
    fmt=opCode[byte][0]
    bLen=opCode[byte][1]
    print("\nByte({}): {:02x}\nLength: {}\nFormat: {}".format(n,byte,bLen,fmt))
#    n += 1
    input()

    if (isBranch(byte)):
        print("IsBranch: True")
        addrStr = str()
        n += i
        dest = code[n]
        addr += bLen
        print("Addr: {:02x} {:02x}".format(addr,dest))
        if (dest < 128):
            addr += dest
        else:
            addr -= (addr - (256 - dest))

        addrStr += "{:02x}".format(addr)
        print("addrStr: {}".format(addrStr))
#        addr += code[n]
#        addrStr = "{:02x}".format(addr)
    else:
        print("IsBranch: False")
        addrStr = str()
        print("addrStr: {}".format(addrStr))
        for i in range(1,bLen):
            n += i
            addr += i
            addrStr += "{:02x}".format(code[n])
            print("addrStr: {}".format(addrStr))
    n += 1

    print("Instruction ({}):{}".format(n,fmt.format(addrStr)))

    # print(fmt.format("{:x}".format(number)))
