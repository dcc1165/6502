PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

E  = %10000000
RW = %01000000
RS = %00100000

  .org $8000

reset:
  lda #%11111111    ; Set all pins on port B to output
  sta DDRB

  lda #%11100000    ; set top 3 pins on por A to output
  sta DDRA

  lda #%00111000    ; Set 8-bit mode; 2-line display; 5x8 font
  sta PORTB

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

  lda #E            ; Set E bit to send instruction
  sta PORTA

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

; Clear the display
  lda #%00000001    ; Display on; cursor on; blink off
  sta PORTB

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

  lda #E            ; Set E bit to send instruction
  sta PORTA

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

; Step 3
  lda #%00001110    ; Display on; cursor on; blink off
  sta PORTB

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

  lda #E            ; Set E bit to send instruction
  sta PORTA

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

; Step 4
  lda #%00000110    ; Increment and shift cursor; don't shift display
  sta PORTB

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

  lda #E            ; Set E bit to send instruction
  sta PORTA

  lda #0            ; Clear RS/RW/E bits
  sta PORTA

; ==========================
; Send message to display
; ==========================
  lda #"H"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"e"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"l"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"l"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"o"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #","
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #$20
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"W"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"o"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"r"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"l"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA

  lda #"d"
  sta PORTB

  lda #RS            ; set RS; Clear RW/E bits
  sta PORTA

  lda #(RS | E)      ; Set E bit to send instruction
  sta PORTA

  lda #RS            ; Clear E bits
  sta PORTA


loop:
  jmp loop

  .org $fffc
  .word reset
  .word $0000
