
 .ORG $8000

;data registers adresses
CNT8=$1000
CNT16L=$1001
CNT16H=$1002
RVAL=$1003
GVAL=$1004
BVAL=$1005
WIDTHL=$1006
WIDTHH=$1007
HEIGHT=$1008
X0L =$1009
X0H =$100A
Y0  =$100B
CHAR=$100C
XCHAR=$100D
YCHAR=$100E
CNT82=$100F
RCHAR=$1010
GCHAR=$1011
BCHAR=$1012
RBACK=$1013
GBACK=$1014
BBACK=$1015
MASK =$1016
ISPUSH=$1017
ISCOLL=$1018
ISNEW =$1019
ISLINE=$101A
CNTLINE=$101B
CNTLINE2=$101C
CNTLINEMEM=$101D
NBLINES=$101E
CINDEXL=$0000
CINDEXH=$0001
BLOC =$1020
NBLOC=$1028
RBLOC=$1030
GBLOC=$1031
BBLOC=$1032
BSEL =$1033
DX   =$1034
DY   =$1035
BROT =$1036
BACT =$1037
MULTMP=$1038
LTMP =$1039
TMP  =$103A
CNT83=$103B
ARRAY=$1100
LINES=$1200
TCNT =$1220
BCNT1=$1221
NCNT1=$1222
TCNT2=$1223
SPEED=$1224
LEVEL=$1225
SCOREL=$1226
SCOREH=$1227
SCOREBET=$1228
;I/O registers
IOA=$6001
IOB=$6000
DDRA=$6003
DDRB=$6002
T1CL=$6004
T1CH=$6005
T1LL=$6006
T1LH=$6007
AUXR=$600B
IFR =$600D
IER =$600E
CR13=$7000
CR20=$7001
MBR1=$7002
LBR1=$7003
MBR2=$7004
LBR2=$7005
MBR3=$7006
LBR3=$7007

;notes

sold4 = $025A
la4   = $0238
si4   = $01FA

do5   = $01DE
re5   = $01AA
mi5   = $017B
fa5   = $0166
sol5  = $013F
sold5 = $012D
la5   = $011C

;durees notes
dc = $01
cr = $02
cp = $03
nr = $04
np = $06
bl = $08
bp = $0C
rd = $10

;8x8 dot matrix font
 .BYTE $FF ;bloc char
 .BYTE $81
 .BYTE $BD
 .BYTE $BD
 .BYTE $BD
 .BYTE $BD
 .BYTE $81
 .BYTE $FF
 .BYTE $99 ;side border char
 .BYTE $99
 .BYTE $99
 .BYTE $99
 .BYTE $99
 .BYTE $99
 .BYTE $99
 .BYTE $99
 .BYTE $99 ;left corner border char
 .BYTE $98
 .BYTE $98
 .BYTE $9F
 .BYTE $9F
 .BYTE $80
 .BYTE $80
 .BYTE $FF
 .BYTE $99 ;right corner border char
 .BYTE $19
 .BYTE $19
 .BYTE $F9
 .BYTE $F9
 .BYTE $01
 .BYTE $01
 .BYTE $FF
 .BYTE $FF ;bottom border char
 .BYTE $00
 .BYTE $00
 .BYTE $FF
 .BYTE $FF
 .BYTE $00
 .BYTE $00
 .BYTE $FF
 .BYTE $7C ;0
 .BYTE $C6
 .BYTE $CE
 .BYTE $D6
 .BYTE $F6
 .BYTE $E6
 .BYTE $7C
 .BYTE $00
 .BYTE $30 ;1
 .BYTE $70
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $FC
 .BYTE $00
 .BYTE $78 ;2
 .BYTE $CC
 .BYTE $0C
 .BYTE $38
 .BYTE $60
 .BYTE $CC
 .BYTE $FC
 .BYTE $00
 .BYTE $78 ;3
 .BYTE $CC
 .BYTE $0C
 .BYTE $38
 .BYTE $0C
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $1C ;4
 .BYTE $3C
 .BYTE $6C
 .BYTE $CC
 .BYTE $FE
 .BYTE $0C
 .BYTE $1E
 .BYTE $00
 .BYTE $FC ;5
 .BYTE $C0
 .BYTE $F8
 .BYTE $0C
 .BYTE $0C
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $38 ;6
 .BYTE $60
 .BYTE $C0
 .BYTE $F8
 .BYTE $CC
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $FC ;7
 .BYTE $CC
 .BYTE $0C
 .BYTE $18
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $00
 .BYTE $78 ;8
 .BYTE $CC
 .BYTE $CC
 .BYTE $78
 .BYTE $CC
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $78 ;9
 .BYTE $CC
 .BYTE $CC
 .BYTE $7C
 .BYTE $0C
 .BYTE $18
 .BYTE $70
 .BYTE $00
 .BYTE $F0 ;L
 .BYTE $60
 .BYTE $60
 .BYTE $60
 .BYTE $62
 .BYTE $66
 .BYTE $FE
 .BYTE $00
 .BYTE $78 ;S
 .BYTE $CC
 .BYTE $E0
 .BYTE $70
 .BYTE $1C
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $00 ;c
 .BYTE $00
 .BYTE $78
 .BYTE $CC
 .BYTE $C0
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $00 ;e
 .BYTE $00
 .BYTE $78
 .BYTE $CC
 .BYTE $FC
 .BYTE $C0
 .BYTE $78
 .BYTE $00
 .BYTE $30 ;i
 .BYTE $00
 .BYTE $70
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $78
 .BYTE $00
 .BYTE $70 ;l
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $30
 .BYTE $78
 .BYTE $00
 .BYTE $00 ;n
 .BYTE $00
 .BYTE $F8
 .BYTE $CC
 .BYTE $CC
 .BYTE $CC
 .BYTE $CC
 .BYTE $00
 .BYTE $00 ;o
 .BYTE $00
 .BYTE $78
 .BYTE $CC
 .BYTE $CC
 .BYTE $CC
 .BYTE $78
 .BYTE $00
 .BYTE $00 ;r
 .BYTE $00
 .BYTE $DC
 .BYTE $76
 .BYTE $66
 .BYTE $60
 .BYTE $F0
 .BYTE $00
 .BYTE $00 ;s
 .BYTE $00
 .BYTE $7C
 .BYTE $C0
 .BYTE $78
 .BYTE $0C
 .BYTE $F8
 .BYTE $00
 .BYTE $00 ;v
 .BYTE $00
 .BYTE $CC
 .BYTE $CC
 .BYTE $CC
 .BYTE $78
 .BYTE $30
 .BYTE $00


NMI        ;Interruption non masquable boutons
 PHA
 LDA #$01
 STA ISPUSH
 PLA
 RTI

ISR        ;Interruption masquable timing
 PHA
 SEI
 INC TCNT
 INC TCNT2
 LDA #$FF
 STA IFR
 ;If time ISR:
 LDA TCNT
 CMP #$0A
 BMI noNewNote
 STZ TCNT
 JSR MusicPlayer
noNewNote
 CLI
 PLA
 RTI

start
 SEI
;disable triggered input
 LDA #$00
 STA AUXR
 STA IOA
;set PORT A and PORT B as output
 LDA #$FF
 STA IOB
 STA DDRA
 LDA #$1F
 STA DDRB
;audio timers init
 LDA #$82
 STA CR13
 LDA #$83
 STA CR20
 LDA #$82
 STA CR13
 LDA #$00
 STA MBR1
 LDA #$00
 STA LBR1
 LDA #$00
 STA MBR2
 LDA #$00
 STA LBR2
 LDA #$00
 STA MBR3
 LDA #$00
 STA LBR3
 STZ TCNT
 STZ BCNT1
 STZ NCNT1
;timing timer init
 LDA #$C0
 STA IER
 LDA #$FF
 STA IFR
 LDA #$40
 STA AUXR
 LDA #$10
 STA T1CL
 LDA #$27
 STA T1CH
 STZ TCNT
;initilize tft lcd screen
;Reset
 JSR delay
 LDA IOB
 AND #$EF   ;RST low
 STA IOB
 ORA #$10   ;RST high
 STA IOB
 JSR delay
 ;Exchange columns/rows, RGB mode
 LDA #$36
 JSR WRTcmd
 LDA #$28
 JSR WRTdata
 ;column number
 LDA #$00
 STA X0H
 LDA #$00
 STA X0L
 LDA #$01
 STA WIDTHH
 LDA #$3F
 STA WIDTHL
 ;row number
 LDA #$00
 STA Y0
 LDA #$EF
 STA HEIGHT
 JSR MakeAddrWindow
 ;Sleep out
 LDA #$11
 JSR WRTcmd
 JSR delay
 ;Display ON
 LDA #$29
 JSR WRTcmd
 ;Clear screen
 JSR clearTFT
 ;clear ARRAY
 LDX #$00
forclarr
 STZ ARRAY,X
 INX
 TXA
 CMP #$FF
 BNE forclarr
 ;
 LDA #$4B
 STA SPEED
 STZ LEVEL
 STZ NBLINES
 STZ SCOREL
 STZ SCOREH
 STZ SCOREBET
 STZ CNTLINE
 STZ CNTLINE2
 STZ CNTLINEMEM
 ;Level
 LDA #$1E
 STA XCHAR
 LDA #$03
 STA YCHAR
 LDA #$00
 JSR WRTtext
 ;Lines
 LDA #$1E
 STA XCHAR
 LDA #$08
 STA YCHAR
 LDA #$01
 JSR WRTtext
 ;Score
 LDA #$1E
 STA XCHAR
 LDA #$0D
 STA YCHAR
 LDA #$02
 JSR WRTtext
 JSR DispNbLines
 JSR DispScore
 JSR DispLevel
 ;
 JSR DrawBorder
 LDA #$01
 STA BSEL
 JSR LoadBloc
 JSR DrawBloc
 STZ ISPUSH
 STZ ISNEW
 STZ ISLINE
 CLI


MainLP
 ;If new bloc has to be placed:
 LDA ISNEW
 BEQ noNew
 JSR NewBloc
noNew
 ;If button is pressed:
 LDA ISPUSH
 BEQ noPress
 JSR ButtonPrgm
noPress
 ;if time ISR
 LDA TCNT2
 CMP SPEED
 BMI noTimeISR
 STZ TCNT2
 JSR TimePRGM
noTimeISR

 JMP MainLP


WRTcmd
 STA IOA
 LDA IOB
 AND #$F1   ;CS, RS, WR low
 STA IOB
 ORA #$02   ;WR high
 STA IOB
 ORA #$08   ;CS high
 STA IOB
 RTS

WRTdata
 STA IOA
 LDA IOB
 AND #$F5   ;CS, WR low
 ORA #$04   ;RS high
 STA IOB
 ORA #$02   ;WR high
 STA IOB
 ORA #$08   ;CS high
 STA IOB
 RTS


delay
 PHX
 PHY
 LDA #0
 TAX
for
 INX
 LDA #0
 TAY
for2d
 INY
 NOP
 TYA
 CMP #$40
 BNE for2d
 TXA
 BNE for
 PLY
 PLX
 RTS


clearTFT
 LDA #$2C
 JSR WRTcmd
 LDA #$00
 STA CNT8
for1
 LDA #$00
 STA CNT16L
 STA CNT16H
for2
 LDA #$00
 JSR WRTdata
 LDA #$00
 JSR WRTdata
 LDA #$00
 JSR WRTdata
 ;for2
 CLC
 LDA CNT16L
 ADC #$01
 STA CNT16L
 LDA CNT16H
 ADC #$00
 STA CNT16H
 BEQ for2
 LDA CNT16L
 CMP #$40
 BNE for2
 ;for1
 LDA CNT8
 INA
 STA CNT8
 CMP #$F0
 BNE for1
 RTS


MakeAddrWindow
 PHX
 ;column number
 LDA #$2A
 JSR WRTcmd
 LDA X0H
 JSR WRTdata ;column space start MSB
 LDA X0L
 JSR WRTdata ;column space start LSB
 CLC
 LDA WIDTHL
 ADC X0L
 TAX
 LDA X0H
 ADC WIDTHH
 JSR WRTdata ;column space end MSB
 TXA
 JSR WRTdata ;column space end LSB
 ;row number
 LDA #$2B
 JSR WRTcmd
 LDA #$00
 JSR WRTdata ;row space start MSB
 LDA Y0
 JSR WRTdata ;row space start LSB
 LDA #$00
 JSR WRTdata ;row space end MSB
 CLC
 LDA HEIGHT
 ADC Y0
 JSR WRTdata ;row space end LSB
 PLX
 RTS


WRTchar
 PHY
;define char window
 ;column number
 CLC
 LDA XCHAR
 ASL
 ASL
 ASL
 STA X0L
 LDA #$00
 ROL
 STA X0H
 LDA #$00
 STA WIDTHH
 LDA #$07
 STA WIDTHL
 ;row number
 LDA YCHAR
 ASL
 ASL
 ASL
 STA Y0
 LDA #$07
 STA HEIGHT
 JSR MakeAddrWindow
;char index
 LDA CHAR
 STA CINDEXL
 STZ CINDEXH
 STZ CNT8
forci
 CLC
 LDA CINDEXL
 ASL
 STA CINDEXL
 LDA CINDEXH
 ROL
 STA CINDEXH
 INC CNT8
 LDA CNT8
 CMP #$03
 BNE forci
 LDA CINDEXH
 ORA #$80
 STA CINDEXH
;draw char
 LDA #$2C
 JSR WRTcmd
 STZ CNT8
forc
 STZ CNT82
 LDA #$80
 STA MASK
 LDY CNT8
forrc
 LDA (CINDEXL),Y
 AND MASK
 BEQ backp
 LDA RCHAR
 JSR WRTdata
 LDA GCHAR
 JSR WRTdata
 LDA BCHAR
 JSR WRTdata
 JMP endp
backp
 LDA RBACK
 JSR WRTdata
 LDA GBACK
 JSR WRTdata
 LDA BBACK
 JSR WRTdata
endp
 CLC
 LDA MASK
 ROR
 STA MASK
 INC CNT82
 LDA CNT82
 CMP #$08
 BNE forrc
 INC CNT8
 LDA CNT8
 CMP #$08
 BNE forc
 PLY
 RTS

DrawBorder
 PHX
 LDA #$DC
 STA RCHAR
 STA GCHAR
 STA BCHAR
 STZ RBACK
 STZ GBACK
 STZ BBACK
 ;sides
 LDX #$00
 LDA #$01
 STA CHAR
forbord
 STX YCHAR
 LDA #$0E
 STA XCHAR
 JSR WRTchar
 LDA #$19
 STA XCHAR
 JSR WRTchar
 INX
 TXA
 CMP #$16
 BNE forbord
 ;left corner
 INC CHAR
 LDA #$0E
 STA XCHAR
 LDA #$16
 STA YCHAR
 JSR WRTchar
 ;right corner
 INC CHAR
 LDA #$19
 STA XCHAR
 JSR WRTchar
 ;bottom
 LDX #$0F
 INC CHAR
forbot
 STX XCHAR
 JSR WRTchar
 INX
 TXA
 CMP #$19
 BNE forbot
 PLX
 RTS

ButtonPrgm
 LDA IOB
 AND #$E0
 CLC
 ROR
 ROR
 ROR
 ROR
 ROR
 CMP #$04
 BNE no4
 JSR LeftBloc
 JMP exc
 RTS
no4
 CMP #$05
 BNE no5
 JSR RotTrigo
 JMP exc
 RTS
no5
 CMP #$06
 BNE no6
 JSR RotClock
 JMP exc
 RTS
no6
 CMP #$07
 BNE no7
 JSR RightBloc
 JMP exc
 RTS
exc
 JSR Collision
 LDA ISCOLL
 BNE no7
 JSR ClearBloc
 JSR CopyBloc
 JSR DrawBloc
no7
 RTS

LoadBloc
 PHX
 PHY
 LDY #$00
 LDA BSEL
 ASL
 ASL
 ASL
 ASL
 ASL
 TAX
forl
 LDA blocs,X
 STA BLOC,Y
 INX
 INY
 LDA blocs,X
 STA BLOC,Y
 INX
 INY
 TYA
 CMP #$08
 BNE forl
 LDA BSEL
 ASL
 ASL
 TAX
 LDA bcolors,X
 STA RBLOC
 INX
 LDA bcolors,X
 STA GBLOC
 INX
 LDA bcolors,X
 STA BBLOC
 JSR ColorBloc
 STZ DX
 STZ DY
 STZ BROT
 PLY
 PLX
 RTS

ColorBloc
 LDA RBLOC
 STA RCHAR
 CLC
 ROR
 STA RBACK
 LDA GBLOC
 STA GCHAR
 CLC
 ROR
 LDA GBACK
 LDA BBLOC
 STA BCHAR
 CLC
 ROR
 STA BBACK
 RTS

DownBloc
 PHX
 LDX #$00
fordown
 LDA BLOC,X
 STA NBLOC,X
 INX
 LDA BLOC,X
 INA
 STA NBLOC,X
 INX
 TXA
 CMP #$08
 BNE fordown
 LDA #$01
 STA BACT
 PLX
 RTS

LeftBloc
 PHX
 LDX #$00
forleft
 LDA BLOC,X
 DEA
 STA NBLOC,X
 INX
 LDA BLOC,X
 STA NBLOC,X
 INX
 TXA
 CMP #$08
 BNE forleft
 LDA #$02
 STA BACT
 PLX
 RTS

RightBloc
 PHX
 LDX #$00
forright
 LDA BLOC,X
 INA
 STA NBLOC,X
 INX
 LDA BLOC,X
 STA NBLOC,X
 INX
 TXA
 CMP #$08
 BNE forright
 LDA #$03
 STA BACT
 PLX
 RTS

DrawBloc
 PHX
 LDX #$00
forb
 LDA BLOC,X
 CLC
 ADC #$0E
 STA XCHAR
 INX
 LDA BLOC,X
 STA YCHAR
 STZ CHAR
 JSR WRTchar
 INX
 TXA
 CMP #$08
 BNE forb
 PLX
 RTS

ClearBloc
 STZ RCHAR
 STZ GCHAR
 STZ BCHAR
 STZ RBACK
 STZ GBACK
 STZ BBACK
 JSR DrawBloc
 JSR ColorBloc
 RTS

RotTrigo
 LDA BSEL
 CMP #$06
 BNE rott
 RTS
rott
 PHX
 PHY
 LDA BROT
 INA
 STA BROT
 CMP #$04
 BNE rt4
 STZ BROT
rt4
 JSR Rotation
 LDA #$04
 STA BACT
 PLY
 PLX
 RTS


RotClock
 LDA BSEL
 CMP #$06
 BNE roth
 RTS
roth
 PHX
 PHY
 LDA BROT
 DEA
 STA BROT
 CMP #$FF
 BNE rh4
 LDA #$03
 STA BROT
rh4
 JSR Rotation
 LDA #$05
 STA BACT
 PLY
 PLX
 RTS

Rotation
 LDA BSEL
 ASL
 ASL
 ASL
 ASL
 ASL
 TAX
 LDY #$00
forrt
 TYA
 CMP BROT
 BEQ endforrt
 CLC
 TXA
 ADC #$08
 TAX
 INY
 JMP forrt
endforrt
 LDY #$00
forrtl
 LDA blocs,X
 CLC
 ADC DX
 STA NBLOC,Y
 INX
 INY
 LDA blocs,X
 CLC
 ADC DY
 STA NBLOC,Y
 INX
 INY
 TYA
 CMP #$08
 BNE forrtl
 RTS

CopyBloc
 PHX
 LDX #$00
forcp
 LDA NBLOC,X
 STA BLOC,X
 INX
 LDA NBLOC,X
 STA BLOC,X
 INX
 TXA
 CMP #$08
 BNE forcp
 PLX
 RTS

Collision
 PHX
 PHY
 STZ ISCOLL
 LDX #$00
forcol
 ;Collision X
 LDA NBLOC,X
 CMP #$00
 BEQ collX
 CMP #$0B
 BEQ collX
 INX
 ;Collision Y
 LDA NBLOC,X
 CMP #$16
 BEQ collY
 ;Collision Bloc
 TAY
 ASL
 ASL
 ASL
 STA MULTMP
 TYA
 ASL
 CLC
 ADC MULTMP
 DEX
 DEC NBLOC,X
 ADC NBLOC,X
 INC NBLOC,X
 TAY
 LDA ARRAY,Y
 BNE collY
 INX
 INX
 TXA
 CMP #$08
 BNE forcol
 JMP noColl
collX
 INC ISCOLL
 JMP endColl
collY
 INC ISCOLL
 LDA ISPUSH
 BNE pushed
 JSR CollisionPRGM
pushed
 JMP endColl
noColl
 LDA BACT
 CMP #$01
 BNE no1
 INC DY
no1
 CMP #$02
 BNE no2
 DEC DX
no2
 CMP #$03
 BNE no3
 INC DX
no3
endColl
 STZ ISPUSH
 PLY
 PLX
 RTS

CollisionPRGM
 PHX
 PHY
 INC ISNEW
 LDX #$00
forMem
 INX
 LDA BLOC,X
 TAY
 ASL
 ASL
 ASL
 STA MULTMP
 TYA
 ASL
 CLC
 ADC MULTMP
 DEX
 DEC BLOC,X
 ADC BLOC,X
 INC BLOC,X
 TAY
 LDA BSEL
 INA
 STA ARRAY,Y
 INX
 INX
 TXA
 CMP #$08
 BNE forMem
 PLY
 PLX
 RTS

TimePRGM
 JSR DownBloc
 JSR Collision
 LDA ISCOLL
 BNE Ycoll
 JSR ClearBloc
 JSR CopyBloc
 JSR DrawBloc
Ycoll
 RTS

NewBloc
 JSR TestLines
 LDA ISLINE
 BEQ noLine
 JSR ClearBlocs
 JSR DeleteLines
 JSR DrawArray
 JSR DispNbLines
 JSR ScoreComp
 JSR DispScore
 JSR DispLevel
 LDA CNTLINE2
 STA CNTLINEMEM
 STZ ISLINE
noLine
 INC BSEL
 LDA BSEL
 CMP #$07
 BNE bok
 STZ BSEL
bok
 STZ ISNEW
 JSR LoadBloc
 JSR DrawBloc
 RTS

TestLines
 PHX
 PHY
 ;All lines filled by default
 LDX #$00
 LDA #01
forClLines
 STA LINES,X
 INX
 TXA
 CMP #$16
 BNE forClLines
 ;
 LDX #$00
forlines
 TXA
 STA LTMP
 ASL
 ASL
 ASL
 STA MULTMP
 TXA
 ASL
 CLC
 ADC MULTMP
 STA MULTMP
 LDY #$00
forbl
 TYA
 CLC
 ADC MULTMP
 TAX
 LDA ARRAY,X
 BEQ noline
 INY
 TYA
 CMP #$0A
 BNE forbl
 LDA #01
 STA ISLINE
 LDA CNTLINE
 CLC
 SED
 ADC #$01
 STA CNTLINE
 CLD
 INC CNTLINE2
ret
 LDA LTMP
 TAX
 INX
 TXA
 CMP #$16
 BNE forlines
 JMP endlines
noline
 LDA LTMP
 TAX
 STZ LINES,X
 JMP ret
endlines
 PLY
 PLX
 RTS

DeleteLines
 PHX
 PHY
 LDX #$00
forlines2
 LDA LINES,X
 BEQ break
 TXA
 DEA
 STA CNT8
fordel
 LDA CNT8
 TAY
 ASL
 ASL
 ASL
 STA MULTMP
 TYA
 ASL
 CLC
 ADC MULTMP
 TAY
 STZ CNT82
forblocs
 LDA ARRAY,Y
 STA TMP
 TYA
 CLC
 ADC #$0A
 TAY
 LDA TMP
 STA ARRAY,Y
 TYA
 SEC
 SBC #$0A
 TAY
 INY
 LDA CNT82
 INA
 STA CNT82
 CMP #$0A
 BNE forblocs

 LDA CNT8
 DEA
 STA CNT8
 BNE fordel

break
 INX
 TXA
 CMP #$16
 BNE forlines2
 PLY
 PLX
 RTS

DrawArray
 PHX
 PHY
 ;Draw array
 STZ CHAR
 STZ CNT83
forY
 LDX #$00
 LDA CNT83
 STA YCHAR
 TAY
 ASL
 ASL
 ASL
 STA MULTMP
 TYA
 ASL
 CLC
 ADC MULTMP
 TAY
forX
 TXA
 STA TMP
 ;define bloc color
 LDA ARRAY,Y
 BEQ brk2
 DEA
 ASL
 ASL
 TAX
 LDA bcolors,X
 STA RCHAR
 CLC
 ROR
 STA RBACK
 INX
 LDA bcolors,X
 STA GCHAR
 CLC
 ROR
 LDA GBACK
 INX
 LDA bcolors,X
 STA BCHAR
 CLC
 ROR
 STA BBACK
 ;draw bloc
 LDA TMP
 TAX
 CLC
 ADC #$0F
 STA XCHAR
 JSR WRTchar
brk2
 INY
 INX
 TXA
 CMP #$0A
 BNE forX

 LDA CNT83
 INA
 STA CNT83
 CMP #$16
 BNE forY
 PLY
 PLX
 RTS

ClearBlocs
 PHX
 PHY
 ;Clear array
 STZ RCHAR
 STZ GCHAR
 STZ BCHAR
 STZ RBACK
 STZ GBACK
 STZ BBACK
 STZ CHAR
 STZ CNT83
forY2
 LDX #$00
 LDA CNT83
 STA YCHAR
 TAY
 ASL
 ASL
 ASL
 STA MULTMP
 TYA
 ASL
 CLC
 ADC MULTMP
 TAY
forX2
 LDA ARRAY,Y
 BEQ brk3
 ;delete bloc
 TXA
 CLC
 ADC #$0F
 STA XCHAR
 JSR WRTchar
brk3
 INY
 INX
 TXA
 CMP #$0A
 BNE forX2

 LDA CNT83
 INA
 STA CNT83
 CMP #$16
 BNE forY2
 PLY
 PLX
 RTS

MusicPlayer
 PHX
 ;test if current note ends
 LDA NCNT1
 TAX
 LDA BCNT1
 INA
 STA BCNT1
 CMP timing1,X
 BNE end1
 STZ BCNT1
;change note
 LDA NCNT1
 ASL
 TAX
 INX
 STZ MBR1
 STZ MBR2
 JSR del
 LDA partition1,X
 STA MBR1
 DEX
 LDA partition1,X
 STA LBR1
;test if partition is finished
 LDA NCNT1
 INA
 STA NCNT1
 CMP #$3B
 BNE end1
 STZ NCNT1
 STZ BCNT1
end1
 PLX
 RTS

del
 PHX
 PHA
 LDX #$00
fordel2
 NOP
 INX
 TXA
 CMP #$64
 BNE fordel2
 PLA
 PLX
 RTS

DispNbLines
 LDA #$20
 STA XCHAR
 LDA #$0A
 STA YCHAR
 LDA CNTLINE
 JSR Disp2digits
 RTS

DispScore
 LDA #$20
 STA XCHAR
 LDA #$10
 STA YCHAR
 LDA SCOREL
 JSR Disp2digits
 LDA SCOREL
 AND #$F0
 BNE is10
 DEC XCHAR
 LDA #$05
 STA CHAR
 JSR WRTchar
is10
 LDA #$1E
 STA XCHAR
 LDA #$10
 STA YCHAR
 LDA SCOREH
 BEQ no100
 JSR Disp2digits
no100
 RTS

DispLevel
 LDA #$20
 STA XCHAR
 LDA #$05
 STA YCHAR
 LDA LEVEL
 JSR Disp2digits
 RTS

Disp2digits
 PHA
 ;Clear count
 STZ RCHAR
 STZ GCHAR
 STZ BCHAR
 STZ RBACK
 STZ GBACK
 STZ BBACK
 STZ CHAR
 JSR WRTchar
 INC XCHAR
 JSR WRTchar
 DEC XCHAR
 LDA #$FF
 STA RCHAR
 STA GCHAR
 STA BCHAR
 ;tens digit
 PLA
 PHA
 AND #$F0
 LSR
 LSR
 LSR
 LSR
 BEQ unit
 CLC
 ADC #$05
 STA CHAR
 JSR WRTchar
 ;units digit
unit
 INC XCHAR
 PLA
 AND #$0F
 CLC
 ADC #$05
 STA CHAR
 JSR WRTchar
 RTS

ScoreComp
 PHX
 LDA CNTLINE2
 SEC
 SBC CNTLINEMEM
 DEA
 STA TMP
 ;10 pts first line
 SED
 LDA SCOREL
 CLC
 ADC #$10
 STA SCOREL
 LDA SCOREH
 ADC #$00
 STA SCOREH
 CLD
 CLC
 LDA SCOREBET
 ADC #$0A
 STA SCOREBET
 LDA TMP
 BEQ only1
 ;15 pts other lines
 TAX
forscore
 SED
 LDA SCOREL
 CLC
 ADC #$15
 STA SCOREL
 LDA SCOREH
 ADC #$00
 STA SCOREH
 CLD
 CLC
 LDA SCOREBET
 ADC #$0F
 STA SCOREBET
 DEX
 TXA
 CMP #$00
 BNE forscore
only1
 ;LEVEL
 LDA SCOREBET
 CMP #$64
 BMI noNewLevel
 LDA LEVEL
 SED
 CLC
 ADC #$01
 STA LEVEL
 CLD
 STZ SCOREBET
 LDA SPEED
 SEC
 SBC #$08
 STA SPEED
noNewLevel
 PLX
 RTS

WRTtext
 PHX
 ASL
 ASL
 ASL
 STA TMP
 TAX
 LDA text,X
 CLC
 ADC TMP
 STA MULTMP
 STZ RBACK
 STZ GBACK
 STZ BBACK
 LDA #$FF
 STA RCHAR
 STA GCHAR
 STA BCHAR
 LDX TMP
 INX
fortxt
 LDA text,X
 STA CHAR
 JSR WRTchar
 INC XCHAR
 INX
 TXA
 CMP MULTMP
 BNE fortxt
 PLX
 RTS

;blocs
blocs
;I-0
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $06, $00
;I-1
 .BYTE $05, $00
 .BYTE $05, $01
 .BYTE $05, $02
 .BYTE $05, $03
;I-2
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $06, $00
;I-3
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $04, $03
;T-0
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $04, $01
;T-1
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $05, $01
;T-2
 .BYTE $03, $01
 .BYTE $04, $01
 .BYTE $05, $01
 .BYTE $04, $00
;T-3
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $03, $01
;L-0
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $03, $01
;L-1
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $05, $02
;L-2
 .BYTE $03, $01
 .BYTE $04, $01
 .BYTE $05, $01
 .BYTE $05, $00
;L-3
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $03, $00
;J-0
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $05, $01
;J-1
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $05, $00
;J-2
 .BYTE $03, $01
 .BYTE $04, $01
 .BYTE $05, $01
 .BYTE $03, $00
;J-3
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $04, $02
 .BYTE $03, $02
;Z-0
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $05, $01
;Z-1
 .BYTE $03, $01
 .BYTE $03, $02
 .BYTE $04, $01
 .BYTE $04, $00
;Z-2
 .BYTE $03, $00
 .BYTE $04, $00
 .BYTE $04, $01
 .BYTE $05, $01
;Z-3
 .BYTE $03, $01
 .BYTE $03, $02
 .BYTE $04, $01
 .BYTE $04, $00
;S-0
 .BYTE $05, $00
 .BYTE $04, $00
 .BYTE $03, $01
 .BYTE $04, $01
;S-1
 .BYTE $03, $00
 .BYTE $03, $01
 .BYTE $04, $01
 .BYTE $04, $02
;S-2
 .BYTE $05, $00
 .BYTE $04, $00
 .BYTE $03, $01
 .BYTE $04, $01
;S-3
 .BYTE $03, $00
 .BYTE $03, $01
 .BYTE $04, $01
 .BYTE $04, $02
;O
 .BYTE $04, $00
 .BYTE $05, $00
 .BYTE $04, $01
 .BYTE $05, $01

bcolors
 .BYTE $00, $FF, $FF, $00
 .BYTE $FF, $FF, $00, $00
 .BYTE $A0, $00, $FF, $00
 .BYTE $FF, $A0, $00, $00
 .BYTE $00, $00, $FF, $00
 .BYTE $FF, $00, $00, $00
 .BYTE $00, $FF, $00, $00

partition1
 .WORD mi5, si4, do5, re5, do5, si4
 .WORD la4, la4, do5, mi5, re5, do5
 .WORD si4, do5, re5, mi5
 .WORD do5, la4, la4, la4, si4, do5

 .WORD re5, fa5, la5, sol5, fa5
 .WORD mi5, do5, mi5, re5, do5
 .WORD si4, si4, do5, re5, mi5
 .WORD do5, la4, la4, 0

 .WORD mi5, do5
 .WORD re5, si4
 .WORD do5, la4
 .WORD sold4, si4, 0
 .WORD mi5, do5
 .WORD re5, si4
 .WORD do5, mi5, la5
 .WORD sold5, 0

timing1
 .BYTE nr, cr, cr, nr, cr, cr
 .BYTE nr, cr, cr, nr, cr, cr
 .BYTE np, cr, nr, nr
 .BYTE nr, nr, cr, cr, cr, cr

 .BYTE np, cr, nr, cr, cr
 .BYTE np, cr, nr, cr, cr
 .BYTE nr, cr, cr, nr, nr
 .BYTE nr, nr, nr, nr

 .BYTE bl, bl
 .BYTE bl, bl
 .BYTE bl, bl
 .BYTE bl, nr, nr
 .BYTE bl, bl
 .BYTE bl, bl
 .BYTE nr, nr, bl
 .BYTE bl, bl

text
 .BYTE $06, $0F, $12, $19, $12, $14, $00, $00
 .BYTE $06, $0F, $13, $15, $12, $18, $00, $00
 .BYTE $06, $10, $11, $16, $17, $12, $00, $00
6502_Tetris.txt
Displaying 6502_Tetris.txt.
