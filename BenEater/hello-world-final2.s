PORTB = $6000          ;VIA port B
PORTA = $6001          ;VIA port A
DDRB = $6002           ;Data Direction Register for port B
DDRA = $6003           ;Data Direction Register for port A

E  = %10000000         ;LCD E register
RW = %01000000         ;LCD RW register
RS = %00100000         ;LCD RS register

  .org $8000           ;Code start at $8000

reset:
  ldx #$ff             ;Set stack pinter to top
  txs

  lda #%11111111       ; Set all pins on port B to output
  sta DDRB

  lda #%11100000       ; set top 3 pins on por A to output
  sta DDRA

  lda #%00111000       ; Set 8-bit mode; 2-line display; 5x8 font
  jsr lcd_instruction

; Step 3
  lda #%00001110       ; Display on; cursor on; blink off
  jsr lcd_instruction

; Step 4
  lda #%00000110       ; Increment and shift cursor; don't shift display
  jsr lcd_instruction

  lda #%00000001       ; Clear the display
  jsr lcd_instruction

; ==========================
; Send message to display
; ==========================

  ldx #0               ;First character of message
print:
  lda message, x       ;load current character to A
  beq loop             ;End?  Jump to infinite loop
  jsr print_char       ;Not End? Print it
  inx                  ;Ready for next character
  jmp print            ;Back to print loop

loop:
  jmp loop

; message:  .asciiz "Hello, World!"
message:  .asciiz "  Line One                              Line Two"

lcd_wait:
  pha                  ;Save A
  lda #%00000000       ;Set Port B to input
  sta DDRB

lcdbusy:
  lda #RW              ;load the LCD RW register
  sta PORTA            ;Send to Port A
  lda #(RW | E)        ;Toggle Enable bit
  sta PORTA            ;Send to Port A
  lda PORTB            ;Read Port B
  and #%10000000       ;LCD busy?
  bne lcdbusy          ;Keep waiting

  lda #RW              ;Reset RW bit
  sta PORTA
  lda #%11111111       ;Set Port B to output
  sta DDRB
  pla                  ;Pull A from stack
  rts

lcd_instruction:
  jsr lcd_wait      ;Wait for LCD to not be busy
  sta PORTB
  lda #0            ;Clear RS/RW/E bits
  sta PORTA
  lda #E            ;Set E bit to send instruction
  sta PORTA         ;write LCD instruction to Port A
  lda #0            ;Clear RS/RW/E bits
  sta PORTA
  rts

print_char:
  jsr lcd_wait       ;Wait for LCD to not be busy
  sta PORTB
  lda #RS            ;set RS; Clear RW/E bits
  sta PORTA
  lda #(RS | E)      ;Set E bit to send instruction
  sta PORTA
  lda #RS            ;Clear E bit
  sta PORTA
  rts

  .org $fffc         ;ROM start vector
  .word reset
  .word $0000
