   lda #$02
   sta $01
   stx $00

loop:
   lda #$01
   jsr pixel

inc00:
   lda $00
   clc
   adc #$01
   sta $00
   bcs inc01
   jmp loop

inc01:
   lda #$ff
   sta $00
   lda #$05
   jsr pixel
   inc $00
   clc
   lda $01
   adc #$01
   sta $01
   cmp #06
   beq done
   jmp loop

pixel:
   sta ($00),y
   rts

done:
   brk
