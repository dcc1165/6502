define base $500
        jsr cls
        ldy #00

loop:
	lda $fe
        tax
        lda base,x
        cmp #$00
        beq loop
        lda #$0
        sta base,x
        iny
        cpy #$00
        beq done
        jmp loop

cls:
        lda #$01
        ldx #$00

l1:
        sta base,x
        inx
        cpx #$00
        bne l1
        jsr sleep
        rts

sleep:
	ldx #$00
        txa
	sta $00

l2:
	inx
        cpx #$ff
        bne l2
        inc $00
        lda $00
        cmp #$05
        bne l2
        rts

done:
        brk
