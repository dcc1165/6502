;  ___           _        __ ___  __ ___
; / __|_ _  __ _| |_____ / /| __|/  \_  )
; \__ \ ' \/ _` | / / -_) _ \__ \ () / /
; |___/_||_\__,_|_\_\___\___/___/\__/___|

; Change direction: W A S D

; $00-01 => screen location of apple, stored as two bytes, where the first
;           byte is the least significant.
; $10-11 => screen location of snake head stored as two bytes
; $12-?? => snake body (in byte pairs)
; $02    => direction ; 1 => up    (bin 0001)
                      ; 2 => right (bin 0010)
                      ; 4 => down  (bin 0100)
                      ; 8 => left  (bin 1000)
; $03    => snake length, in number of bytes, not segments


;The screens is divided in 8 strips of 8x32 "pixels". Each strip
;is stored in a page, having their own most significant byte. Each
;page has 256 bytes, starting at $00 and ending at $ff.

;   ------------------------------------------------------------
;1  | $0200 - $02ff                                            |
;2  |                                                          |
;3  |                                                          |
;4  |                                                          |
;5  |                                                          |
;6  |                                                          |
;7  |                                                          |
;8  |                                                          |
;   ------------------------------------------------------------
;9  | $03 - $03ff                                              |
;10 |                                                          |
;11 |                                                          |
;12 |                                                          |
;13 |                                                          |
;14 |                                                          |
;15 |                                                          |
;16 |                                                          |
;   ------------------------------------------------------------
;17 | $04 - $03ff                                              |
;18 |                                                          |
;19 |                                                          |
;20 |                                                          |
;21 |                                                          |
;22 |                                                          |
;23 |                                                          |
;24 |                                                          |
;   ------------------------------------------------------------
;25 | $05 - $03ff                                              |
;26 |                                                          |
;27 |                                                          |
;28 |                                                          |
;29 |                                                          |
;30 |                                                          |
;31 |                                                          |
;32 |                                                          |
;   ------------------------------------------------------------


define appleL         $00 ; screen location of apple, low byte
define appleH         $01 ; screen location of apple, high byte
define snakeHeadL     $10 ; screen location of snake head, low byte
define snakeHeadH     $11 ; screen location of snake head, high byte
define snakeBodyStart $12 ; start of snake body byte pairs
define snakeDirection $02 ; direction (possible values are below)
define snakeLength    $03 ; snake length, in bytes

; Directions (each using a separate bit)
define movingUp      1
define movingRight   2
define movingDown    4
define movingLeft    8

; ASCII values of keys controlling the snake
define ASCII_w      $77
define ASCII_a      $61
define ASCII_s      $73
define ASCII_d      $64

; System variables
define sysRandom    $fe
define sysLastKey   $ff


  jsr init
  jsr loop

init:
  jsr initSnake
  jsr generateApplePosition
  rts


initSnake:
  ;start the snake in a horizontal position in the middle of the game field
  ;having a total length of one head and 4 bytes for the segments, meaning a
  ;total length of 3: the head and two segments.
  ;The head is looking right, and the snaking moving to the right.

  ;initial snake direction (2 => right)
  lda #movingRight  ;start direction
  sta snakeDirection

  ;initial snake length of 4
  lda #4  ;start length (2 segments)
  sta snakeLength
  
  ;Initial snake head's location's least significant byte to determine
  ;where in a 8x32 strip the head will start. hex $11 is just right
  ;of the center of the first row of a strip
  lda #$11
  sta snakeHeadL
  
  ;Initial snake body, two least significant bytes set to hex $10
  ;and hex $0f, one and two places left of the head respectively
  lda #$10
  sta snakeBodyStart
  
  lda #$0f
  sta $14 ; body segment 1
  
  lda #$04
  sta snakeHeadH
  sta $13 ; body segment 1
  sta $15 ; body segment 2
  rts


generateApplePosition:
  ;The least significant byte of the apple position will determine where
  ;in a 8x32 strip the apple is placed. This number can be any one byte value because
  ;the size of one 8x32 strip fits exactly in one out of 256 bytes
  ;load a new random byte into $00
  lda sysRandom
  sta appleL

  ;load a new random number from 2 to 5 into $01 for the most significant byte of
  ;the apple position. This will determine in which 8x32 strip the apple is placed
  lda sysRandom

  ;AND: logical AND with accumulator. Apply logical AND with hex $03 to value in
  ;register A. Hex 03 is binary 00000011, so only the two least significant bits
  ;are kept, resulting in a value between 0 (bin 00000000) and 3 (bin 00000011).
  ;Add 2 to the result, giving a random value between 2 and 5
  and #$03 ;mask out lowest 2 bits
  clc         ;clear carry flag
  adc #2      ;add 2 to register A, using carry bit for overflow
  sta appleH  ;store A (y coordinate) to address $01

  rts       ;return


loop:
  ;the main game loop
  jsr readKeys        ;jump to subroutine
  jsr checkCollision  ;jump to subroutine
  jsr updateSnake     ;jump to subroutine
  jsr drawApple       ;jump to subroutine
  jsr drawSnake       ;jump to subroutine
  jsr spinWheels      ;jump to subroutine
  jmp loop            ;jump to subroutine


readKeys:
  lda sysLastKey      ;load the value of the latest keypress from address $ff into register A
  cmp #ASCII_w        ;compare value in register A to hex $77 (W)
  beq upKey           ;Branch On Equal, to upKey
  cmp #ASCII_d        ;compare value in register A to hex $64 (D)
  beq rightKey        ;Branch On Equal, to rightKey
  cmp #ASCII_s        ;compare value in register A to hex $73 (S)
  beq downKey         ;Branch On Equal, to downKey
  cmp #ASCII_a        ;compare value in register A to hex $61 (A)
  beq leftKey         ;Branch On Equal, to leftKey
  rts                 ;return

upKey:
  lda #movingDown    ;load value 4 into register A, correspoding to the value for DOWN 
  bit snakeDirection ;AND with value at address $02 (the current direction), 
                     ;setting the zero flag if the result of ANDing the two values
                     ;is 0. So comparing to 4 (bin 0100) only sets zero flag if
                     ;current direction is 4 (DOWN). So for an illegal move (current
                     ;direction is DOWN), the result of an AND would be a non zero value
                     ;so the zero flag would not be set. For a legal move the bit in the
                     ;new direction should not be the same as the one set for DOWN,
                     ;so the zero flag needs to be set
  bne illegalMove    ;Branch If Not Equal: meaning the zero flag is not set.

  lda #movingUp      ;Move is legal, load the value 1 (UP) into
                     ;register A
  sta snakeDirection ;Store the new direction into register A
  rts

rightKey:
  lda #movingLeft    ;load value 8 into register A, corresponding to the value for LEFT
  bit snakeDirection ;AND with current direction and check if result is zero
  bne illegalMove    ;Branch if Not Equal; meaning zero flag is not set

  lda #movingRight   ;Move is legal, load the value 2 (RIGHT) into register A
  sta snakeDirection ;Store the new direction into register A
  rts                ;return

downKey:
  lda #movingUp      ;Load value 1 into register A (UP)
  bit snakeDirection ;AND with current direction at address $02 and check if result
                     ;is zero
  bne illegalMove    ;Branch if Not Equal, meaning zero flag is not set.

  lda #movingDown    ;End up here if move is legal, load #4 into register A
  sta snakeDirection ;Store the new direction into register A
  rts                ;return

leftKey:
  lda #movingRight          ;load value 1 into A (move right)
  bit snakeDirection        ;AND with current direction ($02) and check if result
                            ;is zero
  bne illegalMove           ;Branch If Not Equal (zero flag is not set).

  lda #movingLeft           ;End up here means the moveis lega, load (LEFT) into
                            ;register A
  sta snakeDirection        ;Store the value of A into snakeDirection
  rts                       ;return

illegalMove:
  rts                       ;just return, so the keypress is ignored 


checkCollision:
  jsr checkAppleCollision   ;jump to subroutine checkAppleCollision
  jsr checkSnakeCollision   ;jump to subroutine checkSnakeCollision
  rts                       ;return


checkAppleCollision:
  ;check if the snake collided with the apple by comparing the least significant
  ;and most significant byte of the position of the snake's head and the apple.
  lda appleL
  cmp snakeHeadL
  bne doneCheckingAppleCollision
  lda appleH
  cmp snakeHeadH
  bne doneCheckingAppleCollision

  ;eat apple
  inc snakeLength
  inc snakeLength ;increase length
  jsr generateApplePosition
doneCheckingAppleCollision:
  rts


checkSnakeCollision:
  ldx #2 ;start with second segment
snakeCollisionLoop:
  lda snakeHeadL,x
  cmp snakeHeadL
  bne continueCollisionLoop

maybeCollided:
  lda snakeHeadH,x
  cmp snakeHeadH
  beq didCollide

continueCollisionLoop:
  inx
  inx
  cpx snakeLength          ;got to last section with no collision
  beq didntCollide
  jmp snakeCollisionLoop

didCollide:
  jmp gameOver
didntCollide:
  rts


updateSnake:
  ldx snakeLength
  dex
  txa
updateloop:
  lda snakeHeadL,x
  sta snakeBodyStart,x
  dex
  bpl updateloop

  lda snakeDirection
  lsr
  bcs up
  lsr
  bcs right
  lsr
  bcs down
  lsr
  bcs left
up:
  lda snakeHeadL
  sec
  sbc #$20
  sta snakeHeadL
  bcc upup
  rts
upup:
  dec snakeHeadH
  lda #$1
  cmp snakeHeadH
  beq collision
  rts
right:
  inc snakeHeadL
  lda #$1f
  bit snakeHeadL
  beq collision
  rts
down:
  lda snakeHeadL
  clc
  adc #$20
  sta snakeHeadL
  bcs downdown
  rts
downdown:
  inc snakeHeadH
  lda #$6
  cmp snakeHeadH
  beq collision
  rts
left:
  dec snakeHeadL
  lda snakeHeadL
  and #$1f
  cmp #$1f
  beq collision
  rts
collision:
  jmp gameOver


drawApple:
  ldy #0
  lda sysRandom
  sta (appleL),y
  rts


drawSnake:
  ldx snakeLength
  lda #0
  sta (snakeHeadL,x) ; erase end of tail

  ldx #0
  lda #1
  sta (snakeHeadL,x) ; paint head
  rts


spinWheels:
  ldx #0
spinloop:
  nop
  nop
  dex
  bne spinloop
  rts


gameOver:
