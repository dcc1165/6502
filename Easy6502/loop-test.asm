  cld
  lda #$18
  ldy #$00

loop:
  clc
  iny
  sec
  sbc #$3
  clc
  cmp #$0
  bne loop
  tya
  sta $0f

halt:
  brk
